$('document').ready(function(){
    var mySwiper = new Swiper ('.js-time-table', {
        // Optional parameters
        loop: true,
        autoHeight: true,
        slidesPerView: 4,
        nextButton: '.js-button-next',
        prevButton: '.js-button-prev',

        breakpoints: {
            1280:{
                slidesPerView: 3
            },
            1024:{
                slidesPerView: 2
            },
            720:{
                slidesPerView: 1
            }
        }
    })

    $(".js-select-country, .js-select-city").select2();

    var menu = {
        $menuTrigger: $('.js-show-menu'),
        $mobileNavigation: $('.js-mobile-navigation'),
        $mobileNavigationClose: $('.js-mobile-navigation-close'),
        $overlay: $('.js-overlay'),


        showMenu: function () {
            this.$overlay.fadeIn();
            this.$mobileNavigation.addClass('show');
        },

        hideMenu: function () {
            this.$overlay.fadeOut();
            this.$mobileNavigation.removeClass('show');
        },

        init: function () {
            this.$overlay.on('click', function (e) {
                e.stopPropagation();

                this.hideMenu();
            }.bind(this))

            this.$menuTrigger.on('click', function (e) {
                this.showMenu();

            }.bind(this))

            this.$mobileNavigationClose.on('click', function (e) {
                this.hideMenu();

            }.bind(this))
        }
    }

    var formElements = {
        $formInput: $('.js-form-input'),

        checkInput: function (obj) {
            if( !obj.val() ){
                obj.removeClass('freeze')
            }
            else{
                obj.addClass('freeze')
            }
        },

        init: function () {
            formElements.checkInput( this.$formInput );
            this.$formInput.on('blur', function () {
                formElements.checkInput( $(this) );
            })
        }
    }


    formElements.init();
    menu.init();
});